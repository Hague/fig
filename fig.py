#!/usr/bin/python3

import os
import sys

from configparser import ConfigParser
from typing import List

CURRENT = "current"
DEFAULT = "DEFAULT"

def load_config(config_files : List[str]) -> ConfigParser:
    config = ConfigParser()
    standard_files = [
        os.path.expanduser("~/.config/fig.ini"),
        os.path.expandvars("$XDG_CONFIG_HOME/fig.ini"),
        "fig.ini"
    ]
    config.read(standard_files + config_files)
    return config

def link_app(app : str, mode : str, config : ConfigParser):
    if mode not in config[app]:
        print("{} does not have mode {}".format(app, mode))
        return

    print("Setting {} to mode {}".format(app, mode))

    if CURRENT not in config[app]:
        print("WARNING {} does not have a mode {}".format(app, CURRENT))
        return

    current_link = os.path.expanduser(os.path.expandvars(config[app][CURRENT]))
    mode_file = os.path.expanduser(os.path.expandvars(config[app][mode]))

    if os.path.exists(current_link):
        if not os.path.islink(current_link):
            print("WARNING {} current file exists and is not a link, ignoring app".format(app))
            return
        os.remove(current_link)

    os.symlink(mode_file, current_link)

def link_files(mode : str, config : ConfigParser):
    for app in config:
        if app != DEFAULT:
            link_app(app, mode, config)


if __name__ == "__main__":
    mode = None
    config_files = []
    i = 1
    while i < len(sys.argv):
        if sys.argv[i] == "-c" and i + 1 < len(sys.argv):
            config_files.append(sys.argv[i+1])
            i += 1
        else:
            mode = sys.argv[i]
        i += 1

    if mode is None:
        print("Usage: fig.py -c <ini_file> <config_mode>")
        print("ini_file -- specify a config file to read after standard locations")
        print("config_mode -- which config option to set as current")
        exit(-1)

    config = load_config(config_files)
    link_files(mode, config)
