
# Fig

A simple script for switching config files based on modes.

## Usage

Create a config file in `~/.config/fig.ini` containing a list of apps
and config modes. The special mode `current` is the file that Fig should
create.

## Example Usage

An example for switching between dark and light colorschemes. Set up
`~/.config/fig.ini` to contain:

```
[mutt]
dark = ~/.config/mutt/muttrc.colors.dark
light  = ~/.config/mutt/muttrc.colors.light
current = ~/.config/mutt/muttrc.colors.fig
```

Running

    $ fig.py dark

will create a symlink

    ~/.config/mutt/muttrc.colors.fig -> ~/.config/muttrc/colors.dark

Your `muttrc` file can then

    source muttrc.colors.fig

to read the colors for the current mode.

## Full Example

A full example showing usage with various apps is shown in
[fig.ini](fig.ini).
